#!/bin/bash

# Copia el jar

cp -f java-app/target/*.jar jenkins/build/  

echo "######################"
echo "*** construyendo imagen ***"
echo "######################"

cd jenkins/build/ && docker-compose -f docker-compose-build.yml build --no-cache
